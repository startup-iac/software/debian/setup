#!/bin/bash
KEYRINGS_PATH="/usr/share/keyrings"
ENVIRONMENT_FILE="/etc/profile.d/commons.sh"

# Set environment
echo "export KEYRINGS_PATH=$KEYRINGS_PATH" | sudo tee -a $ENVIRONMENT_FILE
echo "export ENVIRONMENT_FILE=$ENVIRONMENT_FILE" | sudo tee -a $ENVIRONMENT_FILE
source /etc/profile

# Set time & zone
sudo timedatectl set-timezone America/Mexico_City

# Update and Required packages
sudo apt-get update -y && sudo apt-get upgrade -y
sudo apt-get install -y ca-certificates lsb-release gnupg

